from ldif3 import LDIFParser
from pprint import pprint
from requests.auth import HTTPDigestAuth
import requests
import json

parser = LDIFParser(open('/Users/mthahir/WorkSoftware/Data/LDAP/sample2.ldif', 'rb'))
id = 0
uriPrefix = '/ldap/sample/set2/'
headers = {"Content-Type" : "application/json", "WWW-Authenticate": "Basic"}
dhfTransform = '&transform=ml:sjsInputFlow&trans:entity-name=stage&trans:flow-name=loadStage&trans:options={"options":"ldifset2"}&trans:job-id=newLoad'

for dn, entry in parser.parse():
    dict = {}
    if ('objectClass' in entry and 'person' in entry['objectClass']):
        id += 1
        uri = uriPrefix+str(id)+'.json'
        for elemid in entry:
            dict.setdefault(elemid, entry[elemid][0])
#        dict.pop('userpassword')
        jdoc = json.dumps(dict)
        r = requests.put("http://localhost:8010/v1/documents?uri="+uri+dhfTransform, data=jdoc, headers=headers, auth=HTTPDigestAuth('admin','admin'))
        print(id, 'status', r.status_code)
