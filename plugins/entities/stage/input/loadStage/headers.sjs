/*
 * Create Headers Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param content  - the output of your content plugin
 * @param options  - an object containing options. Options are sent from Java
 *
 * @return - an object of headers
 */
function createHeaders(id, content, options) {
  return {
    'rawDataLoad': fn.currentDateTime(),
    'loadedBy': xdmp.getCurrentUser(),
    'rawSource': fn.replace(id, "/([^/]+)/([^.]+)/.+", "$2"),
    'options':options
  };
}

module.exports = {
  createHeaders: createHeaders
};
