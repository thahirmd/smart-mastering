/*~
 * Writer Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param envelope - the final envelope
 * @param options  - an object options. Options are sent from Java
 *
 * @return - nothing
 */
function write(id, envelope, options) {
  
  /* collection name and uriprefix are set in content */
  
/*
  let outputCollName = options.outputCollName;
  let outputUriPrefix = options.outputUriPrefix;
*/
  
  let outputUriPrefix = '/application/';
  let outputCollName = '';
  if (id.toLowerCase().indexOf("Resume".toLowerCase()) >=0){
    outputCollName = "Resume";
    outputUriPrefix += "Resume/";
  } else if (id.toLowerCase().indexOf("BackgroundCheck".toLowerCase()) >=0) {
    outputCollName = "BackgroundCheck";
    outputUriPrefix += "BackgroundCheck/";
  } else {
    outputCollName = "jobAppOther";
    outputUriPrefix += "jobAppOther/";
  };
  
  let lId = fn.replace(fn.replace(id, "/([^/]+)/([^.]+)/([^.]+).+", "$3"), ' ','');
  lUri = outputUriPrefix+ lId+".xml";

  let lCollections = [outputCollName, 'Final'];
  let lPermissions = [xdmp.permission("hr-generalist", "read"),
                      xdmp.permission("hr-manager", "read"),
                      xdmp.permission("data-hub-role", "read"),
                      xdmp.permission("hub-admin-role", "read"),
                      xdmp.permission("pii-reader", "read"),
                      xdmp.permission("hr-generalist", "update"),
                      xdmp.permission("hr-manager", "update"),
                      xdmp.permission("data-hub-role", "update"),
                      xdmp.permission("hub-admin-role", "update")]
  xdmp.documentInsert(lUri, envelope, lPermissions, lCollections);
}
module.exports = write;