'use strict'

/*
* Create Content Plugin
*
* @param id         - the identifier returned by the collector
* @param options    - an object containing options. Options are sent from Java
*
* @return - your content
*/
function createContent(id, options) {
  let doc = cts.doc(id);
/***  
  let colls = xdmp.documentGetCollections(id);
  let outputUriPrefix = '/application/';
  let outputCollName = '';
  if (colls.indexOf("Resume") >=0){
    outputCollName = "Resume";
    outputUriPrefix += "Resume/";
  } else if (colls.indexOf("BackgroundCheck") >=0) {
    outputCollName = "BackgroundCheck";
    outputUriPrefix += "BackgroundCheck/";
  }
  options.outputCollName = outputCollName;
  options.outputUriPrefix = outputUriPrefix;
*/  

  let source;

  // for xml we need to use xpath
  if(doc && xdmp.nodeKind(doc) === 'element' && doc instanceof XMLDocument) {
    source = doc
  }
  // for json we need to return the instance
  else if(doc && doc instanceof Document) {
    source = fn.head(doc.root);
  }
  // for everything else
  else {
    source = doc;
  }

  return extractInstanceJobApplication(source, options);
}
  
/**
* Creates an object instance from some source document.
* @param source  A document or node that contains
*   data for populating a JobApplication
* @return An object with extracted data and
*   metadata about the instance.
*/
function extractInstanceJobApplication(source, options) {
  // the original source documents
  const appProfile = source;

  let enrichedText = '';
  // now check to see if we have XML or json, then create a node clone from the root of the instance
  if (source instanceof Element || source instanceof ObjectNode) {
    let instancePath = '/*:envelope/*:instance';
    if(source instanceof Element) {
      //make sure we grab content root only
      instancePath += '/node()[not(. instance of processing-instruction() or . instance of comment())]';
    }
    source = new NodeBuilder().addNode(fn.head(source.xpath(instancePath))).toNode();
  }
  else{
    source = new NodeBuilder().addNode(fn.head(source)).toNode();
  }
  
  let enrichedNode = enrichContent(source);
  
  let lastName = enrichedNode.xpath('//tag[@val="lastName"]/text()');
  let firstName = enrichedNode.xpath('//tag[@val="firstName"]/text()');
  let appID = -1;
  let msg = '';
  
  if (lastName != ''){
    if (lastName != '' && firstName != '') {
      var $qry = cts.andQuery([cts.collectionQuery('hiring'),
                                                cts.collectionQuery('stage'),
                                                cts.jsonPropertyValueQuery('last_name', lastName),
                                                cts.jsonPropertyValueQuery('first_name', firstName)]);
      }
    else {
      var $qry = cts.andQuery([cts.collectionQuery('hiring'),
                                                cts.collectionQuery('stage'),
                                                cts.jsonPropertyValueQuery('last_name', lastName)]);
    }
    var appDoc = cts.search($qry);
    if (fn.count(appDoc) == 1) {
       let objApp = fn.head(appDoc).root.toObject();
       appID = objApp.envelope.attachments.app_id;
       msg = 'applicant matched';
      }
    else {
      empID = -1
      msg  = 'zero or more than one match with application record';
    }
  }
  // return the instance object

  return {
    '$attachments': enrichedNode,
    '$type': 'JobApplication',
    '$version': '0.0.1',
    'lastName': lastName,
    'firstName' : firstName,
    'appID': appID,
    'appProfile' : appProfile,
    'msg': msg
  };

};


function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}

module.exports = {
  createContent: createContent
};
function enrichContent(source){
  function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}
  var $tokens =  {"GIS":
                       ["spatial", "gml", "ESRI", "MapViewer", "Geospatial", "autocad"],
                "Oracle":
                       ["RAC", "OAS", "Exadata",  "plsql"],
                "NoSQL":
                       ["MarkLogic", "MongoDB", "CouchDB"],
                "Semantics":
                       ["RDF", "graphDB", "graph database"],
                "Hadoop":
                       ["hdfs", "pig","hive"],
                };

  // update tokens with last_name and word_name word lexicons
  var lastNames = new Array();
  var firstNames = new Array();

  for (var result of cts.elementWords([xs.QName("last_name")],'',[], cts.collectionQuery(["hiring"]))){
    lastNames.push(fn.upperCase(result));
  };

  for (var result of cts.elementWords([xs.QName("first_name")],'',[],cts.collectionQuery(["hiring"]))){
    firstNames.push(fn.upperCase(result));
  };
  $tokens.lastName = lastNames.filter(onlyUnique);
  $tokens.firstName = firstNames.filter(onlyUnique);

  var $tokenKeys = Object.keys($tokens);
  var $tokenValues = new Array();
  function getPropertyName(txt){
    var propName = '';
    for (var $key in $tokenKeys) {
         var $propName = $tokenKeys[$key];
         var $propValues = $tokens[$propName];

         for (var i in $propValues) {
           if (cts.contains($propValues[i], cts.wordQuery(txt, ["case-insensitive"]))){
             propName = $propName;
             break;
           }
         }
      if (propName != ''){
        break;
      }
    }
  return propName;
  };

  for (var $key in $tokenKeys) {
     var $propName = $tokenKeys[$key];
     var $propValues = $tokens[$propName];
     for (var i in $propValues) {
      $tokenValues.push($propValues[i])
     }
  };

  var $query = cts.wordQuery($tokenValues, ['case-insensitive'] );

  var $result = new NodeBuilder();
  var $nodeN  = new NodeBuilder().addNode(fn.head(source)).toNode();
  var doc = new NodeBuilder();
  // doc.addNode({"p" : source} )
  doc.startElement("enriched");
  doc.addText(source);
  doc.endElement();

  cts.highlight(doc.toNode(), $query,
                              function(builder,text,node,queries,start) {
                                       builder.startElement("tag");
                                       var prop = getPropertyName(text);
                                       var propVal = '';
                                       var enrchText = '';
                                       if (prop == "lastName" || prop == "firstName") {
                                         propVal = prop;
                                         enrchText = '';
                                       }
                                       else {
                                         propVal = "skills";
                                         enrchText =  prop;
                                       }
                                       builder.addAttribute("val", propVal);
                                       builder.addText(enrchText + " " + text);
                                       builder.endElement();

//                                        builder.addNode({'tag' : getPropertyName(text)})
                                        }, $result);
  return $result.toNode();
}


