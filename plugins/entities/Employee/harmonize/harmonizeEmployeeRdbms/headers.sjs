/*
 * Create Headers Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param content  - the output of your content plugin
 * @param options  - an object containing options. Options are sent from Java
 *
 * @return - an object of headers
 */
function createHeaders(id, content, options) {
  return {
    'lineage': {
      'dataSource': id,
      'harmonization': {
            'date': fn.currentDateTime(),
        	'user': xdmp.getCurrentUser(),
        	'description': 'HR 360 project - legacy data harmonization',
            'entity': options.entity,
            'flowName': options.flow
      }
    },
    "sources": [
        {
          "name": "employee",
          "dateTime": fn.currentDateTime()
        }
      ]
  };
}

module.exports = {
  createHeaders: createHeaders
};
