/*
 * Create Triples Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param content  - the output of your content plugin
 * @param headers  - the output of your heaaders plugin
 * @param options  - an object containing options. Options are sent from Java
 *
 * @return - an array of triples
 */
function createTriples(id, content, headers, options) {
  const empID = content.empID;
  const officeNumber = content.officeNumber;
  const reportsTo = content.reportsTo;
  const fullName = content.fullName;

  const triples = [];
  triples.push(
    sem.triple(
      sem.iri('http://www.marklogic.com/employees#employeeID' + empID),
      sem.iri('http://www.marklogic.com/employees#reportsTo'),
      sem.iri('http://www.marklogic.com/employees#employeeID' + reportsTo)
    )
  )
  triples.push(
    sem.triple(
      sem.iri('http://www.marklogic.com/employees#employeeID' + empID),
      sem.iri('http://www.marklogic.com/employees#inOffice'),
      sem.iri('http://www.marklogic.com/employees#officeNumber' + officeNumber)
    )
  )
  triples.push(
    sem.triple(
      sem.iri('http://www.marklogic.com/employees#employeeID' + empID),
      sem.iri('http://xmlns.com/foaf/0.1/name'),
      fullName
    )
  )
   return triples
 }

module.exports = {
  createTriples: createTriples
};
