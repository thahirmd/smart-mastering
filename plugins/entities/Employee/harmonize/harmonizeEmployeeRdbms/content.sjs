'use strict'

/*
* Create Content Plugin
*
* @param id         - the identifier returned by the collector
* @param options    - an object containing options. Options are sent from Java
*
* @return - your content
*/
function createContent(id, options) {
  let doc = cts.doc(id);

  let source;

  // for xml we need to use xpath
  if(doc && xdmp.nodeKind(doc) === 'element' && doc instanceof XMLDocument) {
    source = doc
  }
  // for json we need to return the instance
  else if(doc && doc instanceof Document) {
    source = fn.head(doc.root);
  }
  // for everything else
  else {
    source = doc;
  }

  return extractInstanceEmployee(source);
}

/**
* Creates an object instance from some source document.
* @param source  A document or node that contains
*   data for populating a Employee
* @return An object with extracted data and
*   metadata about the instance.
*/
function extractInstanceEmployee(source) {
  // the original source documents
  let attachments = null;
  // now check to see if we have XML or json, then create a node clone from the root of the instance
  if (source instanceof Element || source instanceof ObjectNode) {
    let instancePath = '/*:envelope/*:attachments';
    if(source instanceof Element) {
      //make sure we grab content root only
      instancePath += '/node()[not(. instance of processing-instruction() or . instance of comment())]';
    }
    source = new NodeBuilder().addNode(fn.head(source.xpath(instancePath))).toNode();
//    attachments = source;
  }
  else{
    source = new NodeBuilder().addNode(fn.head(source)).toNode();
  }

  let empID = source.emp_id;
  let deptNum = source.dept_num;
  let reportsTo = source.reports_to;
  let officeNumber = source.office_number;
  let fullName = source.first_name + " " + source.last_name;
  let firstName = source.first_name;
  let lastname = source.last_name;
  let dob = xs.date(xdmp.parseDateTime('[M01]/[D01]/[Y0001]', source.dob));
  let addr1 = source.addr1;
  let addr2 = source.addr2;
  let city = source.city;
  let state = source.state;
  let zip = source.zip;
  let homePhone = source.home_phone;
  let mobile = source.mobile;

  let imageURI = fn.concat('/image/', fn.stringJoin([source.first_name, source.last_name, source.emp_id], '-'), '.png').toLowerCase();

// get salary doc for the specific employee being processed
   let salaryDoc = fn.head(cts.search(cts.andQuery([cts.collectionQuery('salary'), cts.collectionQuery('stage'), cts.jsonPropertyValueQuery('emp_id', empID)])));

// get department doc for the specific employee being processed
   let deptDoc = fn.head(cts.search(cts.andQuery([cts.collectionQuery('department'), cts.collectionQuery('stage'), cts.jsonPropertyValueQuery('dept_num', deptNum)])));

// get bank info for the specified employee being processed
   let bankDoc = fn.head(cts.search(cts.andQuery([cts.collectionQuery('bankInfo'), cts.collectionQuery('stage'), cts.jsonPropertyValueQuery('empId', empID)])));

  // get all of the desired data that we wish to denormalize from the salary doc
  let objSal = salaryDoc.root.toObject();
  let objDept = deptDoc.root.toObject();
  let objBank = bankDoc.root.toObject();

  // retrieve relevant info. from Salary document
  let baseSalary = xs.decimal(objSal.envelope.attachments.base_salary);
  let bonus = xs.decimal(objSal.envelope.attachments.bonus);
  let status = xs.string(objSal.envelope.attachments.status);
  let jobEffectiveDate = xs.string(objSal.envelope.attachments.job_effective_date);
  let startDateFinal = xs.date(xdmp.parseDateTime('[M01]/[D01]/[Y0001]', jobEffectiveDate));

  // retrieve relevant info. from Department document
  let deptName = xs.string(objDept.envelope.attachments.dept_name);

  // retrieve bank info from Bank document
  let bankAcct =  xs.decimal(objBank.envelope.attachments.acctId);
  let bankId = xs.decimal(objBank.envelope.attachments.bankId);

  // return the instance object
  return {
    '$attachments': attachments,
    '$type': 'Employee',
    '$version': '0.0.1',
    'base_salary': baseSalary,
    'bonus': bonus,
    'status': status,
    'job_effective_date': jobEffectiveDate,
    'dept_name': deptName,
    'reportsTo': reportsTo,
    'officeNumber': officeNumber,
    'empID': empID,
    'firstName': firstName,
    'lastName': lastname,
    'dob': dob,
    'addr1': addr1,
    'addr2': addr2,
    'city': city,
    'state': state,
    'zip': zip,
    'phone_Home':homePhone,
    'phone_Mobile':mobile,
    'fullName': fullName,
    'employeeImage': imageURI,
    'startDate': startDateFinal,
    'bankAcct': bankAcct,
    'bankId': bankId
  }
};

function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}

module.exports = {
  createContent: createContent
};
