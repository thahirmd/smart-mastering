'use strict'

/*
* Create Content Plugin
*
* @param id         - the identifier returned by the collector
* @param options    - an object containing options. Options are sent from Java
*
* @return - your content
*/
function createContent(id, options) {
  let doc = cts.doc(id);

  let source;

  // for xml we need to use xpath
  if(doc && xdmp.nodeKind(doc) === 'element' && doc instanceof XMLDocument) {
    source = doc
  }
  // for json we need to return the instance
  else if(doc && doc instanceof Document) {
    source = fn.head(doc.root);
  }
  // for everything else
  else {
    source = doc;
  }

  return extractInstanceEmployee(source);
}

/**
* Creates an object instance from some source document.
* @param source  A document or node that contains
*   data for populating a Employee
* @return An object with extracted data and
*   metadata about the instance.
*/
function extractInstanceEmployee(source) {
  // the original source documents
  // let attachments = source;
  let attachments = null;
  // now check to see if we have XML or json, then create a node clone from the root of the instance
  if (source instanceof Element || source instanceof ObjectNode) {
    let instancePath = '/*:envelope/*:attachments';
    if(source instanceof Element) {
      //make sure we grab content root only
      instancePath += '/node()[not(. instance of processing-instruction() or . instance of comment())]';
    }
    source = new NodeBuilder().addNode(fn.head(source.xpath(instancePath))).toNode();
//     attachments = source;
  }
  else{
    source = new NodeBuilder().addNode(fn.head(source)).toNode();
  }

  let bonus = !fn.empty(fn.head(source.xpath('/bonus'))) ? xs.decimal(fn.head(fn.head(source.xpath('/bonus')))) : "";
  let status = !fn.empty(fn.head(source.xpath('/status'))) ? xs.string(fn.head(fn.head(source.xpath('/status')))) : "";
  let deptName = !fn.empty(fn.head(source.xpath('/dept_name'))) ? xs.string(fn.head(fn.head(source.xpath('/dept_name')))) : "";
  let imageURI = !fn.empty(fn.head(source.xpath('/image'))) ? xs.string(fn.head(fn.head(source.xpath('/image')))) : "";
  let reportsTo = !fn.empty(fn.head(source.xpath('/reports_to'))) ? xs.string(fn.head(fn.head(source.xpath('/reports_to')))) : "";
  let officeNumber = !fn.empty(fn.head(source.xpath('/office_no'))) ? xs.string(fn.head(fn.head(source.xpath('/office_no')))) : "";
  let bankAcct = !fn.empty(fn.head(source.xpath('/bankAcct'))) ? xs.string(fn.head(fn.head(source.xpath('/bankAcct')))) : "";
  let bankId = !fn.empty(fn.head(source.xpath('/bankId'))) ? xs.string(fn.head(fn.head(source.xpath('/bankId')))) : "";

  let firstName = !fn.empty(fn.head(source.xpath('/firstName'))) ? xs.string(fn.head(fn.head(source.xpath('/firstName')))) : "";
  let lastname =  !fn.empty(fn.head(source.xpath('/lastName'))) ? xs.string(fn.head(fn.head(source.xpath('/lastName')))) : "";
  let db = !fn.empty(fn.head(source.xpath('/dateOfBirth'))) ? xs.string(fn.head(fn.head(source.xpath('/dateOfBirth')))) : "";
  let dob = xs.date(xdmp.parseDateTime('[M01]/[D01]/[Y0001]', db));
  let addr1 = !fn.empty(fn.head(source.xpath('/addr1'))) ? xs.string(fn.head(fn.head(source.xpath('/addr1')))) : "";
  let addr2 = !fn.empty(fn.head(source.xpath('/addr2'))) ? xs.string(fn.head(fn.head(source.xpath('/addr2')))) : "";
  let city = !fn.empty(fn.head(source.xpath('/city'))) ? xs.string(fn.head(fn.head(source.xpath('/city')))) : "";
  let state = !fn.empty(fn.head(source.xpath('/state'))) ? xs.string(fn.head(fn.head(source.xpath('/state')))) : "";
  let zip = !fn.empty(fn.head(source.xpath('/zip'))) ? xs.string(fn.head(fn.head(source.xpath('/zip')))) : "";
  let homePhone = !fn.empty(fn.head(source.xpath('/homePhone'))) ? xs.string(fn.head(fn.head(source.xpath('/homePhone')))) : "";
  let mobile = !fn.empty(fn.head(source.xpath('/mobile'))) ? xs.string(fn.head(fn.head(source.xpath('/mobile')))) : "";
  let empID = source.id;
  let fullName = source.firstName + " " + source.lastName;

  let salaryHistory =  source.salaryHistory;
  let len = salaryHistory.length;
  let jobEffectiveDate = len!=0 ? salaryHistory[len-1].effectiveDate : null;
  let baseSalary = len!=0 ? salaryHistory[len-1].salary : null;
  let startDate = len!=0 ? salaryHistory[0].effectiveDate : null;
  let startDateFinal = xs.date(xdmp.parseDateTime('[M01]/[D01]/[Y0001]', startDate));


  // return the instance object
  return {
    '$attachments': attachments,
    '$type': 'Employee',
    '$version': '0.0.1',
    'base_salary': baseSalary,
    'bonus': bonus,
    'status': status,
    'job_effective_date': jobEffectiveDate,
    'dept_name': deptName,
    'reportsTo': reportsTo,
    'officeNumber': officeNumber,
    'empID': empID,
    'firstName': firstName,
    'lastName': lastname,
    'dob': dob,
    'addr1': addr1,
    'addr2': addr2,
    'city': city,
    'state': state,
    'zip': zip,
    'phone_Home':homePhone,
    'phone_Mobile':mobile,
    'fullName': fullName,
    'employeeImage': imageURI,
    'startDate': startDateFinal,
    'bankAcct': bankAcct,
    'bankId' :bankId
  }
};


function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}

module.exports = {
  createContent: createContent
};
