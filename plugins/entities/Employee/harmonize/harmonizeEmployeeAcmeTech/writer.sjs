/*~
 * Writer Plugin
 *
 * @param id       - the identifier returned by the collector
 * @param envelope - the final envelope
 * @param options  - an object options. Options are sent from Java
 *
 * @return - nothing
 */
function write(id, envelope, options) {
  let lUri = fn.replace(id, "/stage", '');
  let lCollections = [options.entity, 'Final', 'Employee', 'Acme']
  let lPermissions = [xdmp.permission("hr-generalist", "read"),
                      xdmp.permission("hr-manager", "read"),
                      xdmp.permission("data-hub-role", "read"),
                      xdmp.permission("hub-admin-role", "read"),
                      xdmp.permission("pii-reader", "read"),
                      xdmp.permission("hr-acme", "read"),
                      xdmp.permission("hr-generalist", "update"),
                      xdmp.permission("hr-manager", "update"),
                      xdmp.permission("data-hub-role", "update"),
                      xdmp.permission("hub-admin-role", "update"),
                      xdmp.permission("hr-acme", "update")
                    ]
  xdmp.documentInsert(lUri, envelope, lPermissions, lCollections);
}

module.exports = write;
