function readEmployee(context, params, content) {
  let instanceData = content.xpath('/envelope/instance');
  let attachmentData = content.xpath('/envelope/attachments');
  let transformedData =
  {
    'employee': {
      instanceData,
      attachmentData
    }
  };
  context.outputType = "application/json";
  return transformedData;
}
exports.transform = readEmployee;
