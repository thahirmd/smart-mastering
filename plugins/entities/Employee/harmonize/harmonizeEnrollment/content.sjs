'use strict'

/*
* Create Content Plugin
*
* @param id         - the identifier returned by the collector
* @param options    - an object containing options. Options are sent from Java
*
* @return - your content
*/
function createContent(id, options) {
  let doc = cts.doc(id);

  let source;

  // for xml we need to use xpath
  if(doc && xdmp.nodeKind(doc) === 'element' && doc instanceof XMLDocument) {
    source = doc
  }
  // for json we need to return the instance
  else if(doc && doc instanceof Document) {
    source = fn.head(doc.root);
  }
  // for everything else
  else {
    source = doc;
  }

  return extractInstanceEmployee(source);
}

/**
* Creates an object instance from some source document.
* @param source  A document or node that contains
*   data for populating a Employee
* @return An object with extracted data and
*   metadata about the instance.
*/
function extractInstanceEmployee(source) {
  // the original source documents
  // let attachments = source;
  let attachments = null;
  // now check to see if we have XML or json, then create a node clone from the root of the instance
  if (source instanceof Element || source instanceof ObjectNode) {
    let instancePath = '/*:envelope/*:attachments';
    if(source instanceof Element) {
      //make sure we grab content root only
      instancePath += '/node()[not(. instance of processing-instruction() or . instance of comment())]';
    }
    source = new NodeBuilder().addNode(fn.head(source.xpath(instancePath))).toNode();
//     attachments = source;
  }
  else{
    source = new NodeBuilder().addNode(fn.head(source)).toNode();
  }
  let memID = source.memId;
  let firstName = source.first_name;
  let lastname = source.last_name;
  let dob = xs.date(xdmp.parseDateTime('[M]/[D]/[Y]', source.dob));
  let addr1 = source.addr1;
  let addr2 = source.addr2;
  let city = source.city;
  let state = source.state;
  let zip = source.zip;
  let homePhone = source.home_phone;
  let mobile = source.mobile;
  let userId  = source.userid;

  // return the instance object
  return {
    '$attachments': attachments,
    '$type': 'Employee',
    '$version': '0.0.1',
    'memID': memID,
    'firstName': firstName,
    'lastName': lastname,
    'dob': dob,
    'addr1': addr1,
    'addr2': addr2,
    'city': city,
    'state': state,
    'zip': zip,
    'phone_Home':homePhone,
    'phone_Mobile':mobile,
    'userId': userId
  }
};


function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}

module.exports = {
  createContent: createContent
};
