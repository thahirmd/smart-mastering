'use strict'

/*
* Create Content Plugin
*
* @param id         - the identifier returned by the collector
* @param options    - an object containing options. Options are sent from Java
*
* @return - your content
*/
function createContent(id, options) {
  let doc = cts.doc(id);

  let source;

  // for xml we need to use xpath
  if(doc && xdmp.nodeKind(doc) === 'element' && doc instanceof XMLDocument) {
    source = doc
  }
  // for json we need to return the instance
  else if(doc && doc instanceof Document) {
    source = fn.head(doc.root);
  }
  // for everything else
  else {
    source = doc;
  }

  return extractInstanceBankTxn(id, source);
}
  
/**
* Creates an object instance from some source document.
* @param source  A document or node that contains
*   data for populating a BankTxn
* @return An object with extracted data and
*   metadata about the instance.
*/
function extractInstanceBankTxn(id, source) {
  const jsearch = require('/MarkLogic/jsearch');
  // the original source documents
  var attachments = [];
  var uriList = [];
  attachments.push(source.envelope.attachments);
  uriList.push(makeReferenceObject('uri',id));
  var empID = source.envelope.attachments.empID;
  // now check to see if we have XML or json, then create a node clone from the root of the instance
  if (source instanceof Element || source instanceof ObjectNode) {
    let instancePath = '/*:envelope/*:instance';
    if(source instanceof Element) {
      //make sure we grab content root only
      instancePath += '/node()[not(. instance of processing-instruction() or . instance of comment())]';
    }
    source = new NodeBuilder().addNode(fn.head(source.xpath(instancePath))).toNode();
  }
  else{
    source = new NodeBuilder().addNode(fn.head(source)).toNode();
  }
  
  /* let bankTxn = fn.head(cts.search(cts.andQuery([cts.collectionQuery('bankConfrm'), cts.jsonPropertyValueQuery('refId', empID)]))); 
 */
  var bankTxns =  jsearch
                    .collections('bankConfrm')
                    .documents()
                    .where(
                        jsearch.byExample({
                                          'refId': empID
                                          })
                    )
                    .result('value')
                    .results.map(function(doc) {
                                    return {uri:doc.uri,  content: doc.document.envelope.attachments};
                                 })

  for (var i = 0; i < bankTxns.length; i++) {
 	 	var bankTxn = bankTxns[i];
  	attachments.push(bankTxn.content)
  	uriList.push(makeReferenceObject('uri',bankTxn.uri))
  }
  // return the instance object
  return {
    '$attachments': attachments,
    '$type': 'payCheck',
    '$version': '0.0.1',
    'Sources':uriList
  }
};


function makeReferenceObject(type, ref) {
  return {
    '$type': type,
    '$ref': ref
  };
}
module.exports = {
  createContent: createContent
};

